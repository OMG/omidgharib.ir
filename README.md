## My Own website
The website for the Omid Gharib


### Contributing

* Check our projec's website and report any bugs or ideas in [issues](https://github.com/omidgharib/omidgharib.ir/issues)

* Clone this project and check source code
```
    git clone https://github.com/omidgharib/omidgharib.ir.git
```

* Fork it for your self and do some develope and pull request it to us :smiley:


### Buliding Technologies
* [HTML5](http://ali.md/wiki/html5)
* [CSS3](http://ali.md/css3ref)
* [1Styles](http://ali.md/1styles)
* [JavaScript](http://ali.md/wiki/javascript)
* [Zepto.js](http://ali.md/zepto.js)
* [jQuery.js](http://ali.md/jquery.js)
* [1scripts.js](http://ali.md/1scripts.js)
* [html5shiv](http://ali.md/html5shiv)
* [PHP](http://ali.md/php/)
* [MySQL](http://ali.md/wiki/mysql)


### The Team
* [Omid Gharib](https://github.com/omidgharib) Developer /Programmer

### Our Desire Design : [yootheme](http://www.yootheme.com)

### All rights reserved ###
Please note that omidgharib.ir is a curated site and that we reserve the right to refuse any listing for any reason.

The [omidgharib.ir](http://omidgharib.ir) code and website is released under a [MIT License](http://opensource.org/licenses/MIT).


### A Omid Gharib website
Produced by Omid Gharib

